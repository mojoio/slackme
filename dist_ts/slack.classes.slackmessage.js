"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SlackMessage = void 0;
const plugins = __importStar(require("./slack.plugins"));
class SlackMessage {
    constructor(slackAccountArg, messageOptionsArg) {
        this.requestRunning = plugins.smartpromise.defer();
        if (slackAccountArg) {
            this.slackAccountRef = slackAccountArg;
        }
        this.messageOptions = messageOptionsArg;
        this.requestRunning.resolve();
    }
    async updateAndSend(messageOptionsArg) {
        this.messageOptions = messageOptionsArg;
        await this.sendToRoom(this.channel, 'update');
    }
    async startThread(messageOptionsArg) {
        this.messageOptions = messageOptionsArg;
        this.sendToRoom(this.channel, 'threaded');
    }
    async sendToRoom(channelNameArg, modeArg = 'new') {
        this.channel = channelNameArg;
        if (this.slackAccountRef) {
            const response = await this.slackAccountRef.sendMessage({
                channelArg: this.channel,
                messageOptions: this.messageOptions,
                mode: modeArg,
                ts: this.ts
            });
            if (modeArg === 'new') {
                this.ts = response.body.message.ts;
                this.channel = response.body.channel;
            }
        }
        else {
            throw new Error('you need to set a slackRef before sending the message!');
        }
    }
}
exports.SlackMessage = SlackMessage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xhY2suY2xhc3Nlcy5zbGFja21lc3NhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi90cy9zbGFjay5jbGFzc2VzLnNsYWNrbWVzc2FnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseURBQTJDO0FBc0QzQyxNQUFhLFlBQVk7SUFRdkIsWUFBWSxlQUE2QixFQUFFLGlCQUFrQztRQUZ0RSxtQkFBYyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFHbkQsSUFBSSxlQUFlLEVBQUU7WUFDbkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7U0FDeEM7UUFDRCxJQUFJLENBQUMsY0FBYyxHQUFHLGlCQUFpQixDQUFDO1FBQ3hDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELEtBQUssQ0FBQyxhQUFhLENBQUMsaUJBQWtDO1FBQ3BELElBQUksQ0FBQyxjQUFjLEdBQUcsaUJBQWlCLENBQUM7UUFDeEMsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELEtBQUssQ0FBQyxXQUFXLENBQUMsaUJBQWtDO1FBQ2xELElBQUksQ0FBQyxjQUFjLEdBQUcsaUJBQWlCLENBQUM7UUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxLQUFLLENBQUMsVUFBVSxDQUFDLGNBQXNCLEVBQUUsVUFBeUMsS0FBSztRQUNyRixJQUFJLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQztRQUM5QixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsTUFBTSxRQUFRLEdBQUcsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQztnQkFDdEQsVUFBVSxFQUFFLElBQUksQ0FBQyxPQUFPO2dCQUN4QixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7Z0JBQ25DLElBQUksRUFBRSxPQUFPO2dCQUNiLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTthQUNaLENBQUMsQ0FBQztZQUNILElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDckIsSUFBSSxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDdEM7U0FDRjthQUFNO1lBQ0wsTUFBTSxJQUFJLEtBQUssQ0FBQyx3REFBd0QsQ0FBQyxDQUFDO1NBQzNFO0lBQ0gsQ0FBQztDQUNGO0FBM0NELG9DQTJDQyJ9